//
//  CustomerEntity.h
//  NuLook
//
//

#import <Foundation/Foundation.h>

@interface CustomerEntity : NSObject

@property (nonatomic) int _idx;

@property (nonatomic) int _userid;

@property (nonatomic, strong) NSString *_company;

@property (nonatomic, strong) NSString * _contact;

@property (nonatomic, strong) NSString * _address;

@property (nonatomic, strong) NSString * _city;

@property (nonatomic, strong) NSString * _phone1;

@property (nonatomic, strong) NSString * _phone2;

@property (nonatomic, strong) NSString * _email;

@end
