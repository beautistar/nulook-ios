//
//  OrderEntity.h
//  NuLook
//
//  Created by AOC on 06/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderEntity : NSObject

@property (nonatomic) int _idx;

@property (nonatomic, strong) NSString *_style;

@property (nonatomic, strong) NSString * _color;

@property (nonatomic, strong) NSString * _description;

@property (nonatomic) int _s;

@property (nonatomic) int _m;

@property (nonatomic) int _l;

@property (nonatomic) int _xl;

@property (nonatomic) int _1xl;

@property (nonatomic) int _2xl;

@property (nonatomic) int _3xl;

@property (nonatomic) int _qty;

@property (nonatomic) float _price;

@property (nonatomic) float _disc;

@property (nonatomic) int _total;

@end
