//
//  CustomerEntity.m
//  NuLook
//
//

#import "CustomerEntity.h"

@implementation CustomerEntity

@synthesize _idx, _userid, _address, _city, _email, _phone1, _phone2, _company, _contact;


- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _idx = 0;
        _userid = 0;
        _company = @"";
        _contact = @"";
        _address = @"";
        _city = @"";
        _phone1 = @"";
        _phone2 = @"";
        _email = @"";
    }
    
    return self;
}

@end
