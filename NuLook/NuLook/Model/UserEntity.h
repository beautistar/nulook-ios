//
//  UserEntity.h
//  NuLook
//
//  Created by AOC on 04/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEntity : NSObject

@property (nonatomic) int _idx;

@property (nonatomic, strong) NSString *_username;

@property (nonatomic, strong) NSString * _password;

@property (nonatomic) int _permission;

- (BOOL) isValid;

@end
