//
//  UserEntity.m
//  NuLook
//
//  Created by AOC on 04/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize _idx, _username, _password, _permission;


- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _idx = 0;
        _username = @"";
        _password = @"";
        _permission = 0;
        
    }
    
    return self;
}

- (BOOL) isValid {
    
    if(_idx > 0) {
        
        return YES;
    }
    
    return NO;
}


@end
