//
//  OrderEntity.m
//  NuLook
//
//  Created by AOC on 06/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "OrderEntity.h"

@implementation OrderEntity

@synthesize _idx, _style, _color, _description, _s, _l, _m, _xl, _1xl, _2xl, _3xl, _qty, _price, _disc, _total;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _idx = 0;
        _style = @"";
        _description = @"";
        _s = 0;
        _l = 0;
        _m = 0;
        _xl = 0;
        _1xl = 0;
        _2xl = 0;
        _3xl = 0;
        _qty = 0;
        _price = 0;
        _disc = 0;
        _total = 0;
    }
    
    return self;
}


@end
