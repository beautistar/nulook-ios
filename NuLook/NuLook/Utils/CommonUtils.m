//
//  CommonUtils.m
//  NuLook
//

#import "CommonUtils.h"
#import "AppDelegate.h"
#import "UserDefault.h"

@implementation CommonUtils


+ (void) saveUserInfo {
    
    [CommonUtils setUserIdx:APPDELEGATE.Me._idx];
    [CommonUtils setUserName:APPDELEGATE.Me._username];
    [CommonUtils setUserPassword:APPDELEGATE.Me._password];
    [CommonUtils setUserPermission:APPDELEGATE.Me._permission];

    
}

+ (void) loadUserInfo {
    
    if(APPDELEGATE.Me) {
        
        APPDELEGATE.Me._idx = [CommonUtils getUserIdx];
        APPDELEGATE.Me._username = [CommonUtils getUserName];
        APPDELEGATE.Me._password = [CommonUtils getUserPassword];
        APPDELEGATE.Me._permission = [CommonUtils getUserPermission];
    }    
}

+ (void) setUserIdx:(int) idx {
    
    [UserDefault setIntValue:PREFKEY_ID value:idx];    
}

+ (int) getUserIdx {
    
    return [UserDefault getIntValue:PREFKEY_ID];
}

+ (void) setUserName: (NSString *) name {
    
    [UserDefault setStringValue:PREFKEY_USERNAME value:name];
}

+ (NSString *) getUserName {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_USERNAME];
    return res == nil ? @"" : res;
    
}

+ (void) setUserPassword: (NSString *) password {
    
    [UserDefault setStringValue:PREFKEY_PASSWORD value:password];
    
}

+ (NSString *) getUserPassword {
    
    return @"";   NSString *res = [UserDefault getStringValue:PREFKEY_PASSWORD];
    return res == nil ? @"" : res;
    
}

+ (void) setUserPermission: (int) permission {
    
    [UserDefault setIntValue:PREFKEY_PERMISSION value:permission];
    
}

+ (int) getUserPermission {
    
    return [UserDefault getIntValue:PREFKEY_PERMISSION];
    
}

+ (void) setUserLogin:(BOOL) isLoggedIn {
    
    [UserDefault setBoolValue:PREFKEY_LOGGED_IN value:isLoggedIn];
    
}

+ (BOOL) getUserLogin {
    
    return [UserDefault getBoolValue:PREFKEY_LOGGED_IN];
}

@end
