//
//  Const.h
//  LIVEASE
//

#import <Foundation/Foundation.h>
#import "OrderEntity.h"

@interface Const : NSObject

extern int STYLE;
extern NSString * COLOR;
extern float PRICE;
extern int PPK_NO;
extern NSString *S;
extern NSString *NOTE;
extern int BACK_WHERE;
extern int _from;
extern int deviceType;

extern NSMutableArray *ORDERLIST;
extern NSMutableArray *CUSTOMER_LIST;
extern NSMutableArray *CUSTOMER_MENU;

// int const

#define ADMIN_PERMISSION    1
#define USER_PERMISSION     0

#define S_MIN               10
#define M_MIN               20
#define L_MIN               30
#define XL3_MIN             40
#define XL_MIN              50
#define XXL_MIN             60
#define XXXL_MIN            70
#define PPK_MIN             80

#define S_MAX               100
#define M_MAX               200
#define L_MAX               300
#define XL3_MAX             400
#define XL_MAX              500
#define XXL_MAX             600
#define XXXL_MAX            700
#define PPK_MAX             800

#define FROM_BACK           111
#define FROM_CONFIRM        222
#define FROM_HOME           333
#define FROM_SALE           444

#define IPHONE4             666
#define IPHONE              777
#define IPAD                888

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

// string const

#define CONN_ERROR                          @"Connect server fail.\n Please try again when you are online."

#define ALERT_TITLE                         @"NuLook"
#define ALERT_OK                            @"OK"
#define ALERT_CANCEL                        @"Cancel"

#define SELECT_ITEM                         @"Please select item to remove."

#define INPUT_USERNAME                      @"Please input User Name."
#define INPUT_PWD                           @"Please input Password."

#define INPUT_COMPANY                       @"Please input company."
#define INPUT_PHONE1                        @"Please input phone number 1."
#define INVALID_EDITCUSTOMER                @"Invalid to edit customer."
#define INVALID_DELCUSTOMER                 @"Invalid to delete customer."
#define INVALID_STYLECODE                   @"Style code never be blank."
#define INVALID_CUSTOMER                    @"Customer never be blank."
#define INVALID_PRICE                         @"Price never be blank."
#define INVALID_COLOR                       @"Color never be blank"
#define INVALID_QTY                         @"Quantity never be blank"
#define INPUT_NOTE                          @"Input note."
#define INPUT_MAIL                          @"Input mail"


#define RES_UNREGISTERED_USERNAME           @"Unregistered username."
#define CHECK_PWD                           @"Wrong password."
#define EXIST_CUS_NAME                      @"Customer company or phone1 already exist."
#define CONFIRM_CANCEL                      @"Are you sure you want to cancel order?"

@end
