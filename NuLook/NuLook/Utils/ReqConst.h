//
//  ReqConst.h
//  NuLook
//
//

#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define SERVER_BASE_URL                 @"http://52.43.164.188"

//#define SERVER_BASE_URL                 @"http://192.168.1.180/NuLook"


#define SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]


#define REQ_LOGIN                       @"login"
#define REQ_GET_CUSTOMER                @"getCustomer"
#define REQ_GET_ALL_CUSTOMER            @"getAllCustomer"
#define REQ_ADD_CUSTOMER                @"addCustomer"
#define REQ_EDIT_CUSTOMER               @"editCustomer"
#define REQ_DEL_CUSTOMER                @"deleteCustomer"
#define REQ_SUBMIT_ORDER                @"submitOrder"
#define REQ_SENDINVOICE                 @"sendInvoice"



#pragma mark -
#pragma mark - Response Params

#define RES_CODE                        @"result_code"

#define RES_IDX                         @"idx"
#define RES_USERNAME                    @"username"
#define RES_PERMISSION                  @"permission"

#define RES_CUSTOMER_INFOS              @"customer_infos"
#define CUSTOMER_IDX                    @"customer_id"
#define USER_IDX                        @"user_id"
#define COMPANY                         @"company"
#define CONTACT                         @"contact"
#define ADDRESS                         @"address"
#define CITY                            @"city"
#define PHONE1                          @"phone1"
#define PHONE2                          @"phone2"
#define EMAIL                           @"email"


#define PARAM_ORDER_LIST                @"order_list"
#define PARAM_STYLE                     @"style"
#define PARAM_CUSTOMER                  @"customer"
#define PARAM_COLOR                     @"color"
#define PARAM_DESCRIPTION               @"description"
#define PARAM_S                         @"s"
#define PARAM_M                         @"m"
#define PARAM_L                         @"l"
#define PARAM_XL                        @"xl"
#define PARAM_XL1                       @"1xl"
#define PARAM_XL2                       @"2xl"
#define PARAM_XL3                       @"3xl"
#define PARAM_QTY                       @"qty"
#define PARAM_PRICE                     @"price"
#define PARAM_DISC                      @"disc"
#define PARAM_TOTAL                     @"total"
#define PARAM_NOTE                      @"note"
#define PARAM_CUSTOMER                  @"customer"



/**
 **     Response Code
 **/

#pragma mark -
#pragma mark - Response Code

#define CODE_SUCCESS                    0
#define CODE_UNREGISTER_USERANEM        101
#define CODE_WRONG_PWD                  102
#define CODE_USERNAME_EXIST             103


#endif /* ReqConst_h */
