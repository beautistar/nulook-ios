//
//  PrefConst.h
//  NuLook
//
//  Created by AOC on 04/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_ID              @"user_id"
#define PREFKEY_USERNAME        @"user_name"
#define PREFKEY_PERMISSION      @"user_permission"
#define PREFKEY_PASSWORD        @"user_password"
#define PREFKEY_LOGGED_IN       @"user_logged_in"

#endif /* PrefConst_h */
