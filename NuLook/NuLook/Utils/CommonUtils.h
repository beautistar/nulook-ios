//
//  CommonUtils.h
//  NuLook
//

#import <Foundation/Foundation.h>
#import "Const.h"
#import "ReqConst.h"
#import "NSString+Encode.h"
#import "AppDelegate.h"
#import <AFNetworking/AFNetworking.h>
//#import <JLToast/JLToast-Swift.h>
#import <Toast/UIView+Toast.h>

@interface CommonUtils : NSObject

+ (void) saveUserInfo;
+ (void) loadUserInfo;

+ (void) setUserIdx:(int) idx;
+ (int) getUserIdx;

+ (void) setUserName: (NSString *) name;
+ (NSString *) getUserName;

+ (void) setUserPassword: (NSString *) password;
+ (NSString *) getUserPassword;

+ (void) setUserPermission: (int) permission;
+ (int) getUserPermission;

+ (void) setUserLogin:(BOOL) isLoggedIn;
+ (BOOL) getUserLogin;

@end
