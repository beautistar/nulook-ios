//
//  CustomerCell.m
//  NuLook
//

#import "CustomerCell.h"

@implementation CustomerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setCustomerName:(NSString *) name {
    
    self.tfCustomerName.text = name;
    
    
}

@end
