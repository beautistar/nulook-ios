//
//  CustomerCell.h
//  NuLook
//
//

#import <UIKit/UIKit.h>

@interface CustomerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tfCustomerName;


- (void) setCustomerName:(NSString *) name;

@end
