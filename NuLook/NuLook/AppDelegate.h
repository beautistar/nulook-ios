//
//  AppDelegate.h
//  NuLook
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UserEntity *Me;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UserEntity * Me;


@end

