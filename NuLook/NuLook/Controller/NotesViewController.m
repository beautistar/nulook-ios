//
//  NotesViewController.m
//  NuLook
//
//  Created by AOC on 29/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "NotesViewController.h"
#import "UITextView+Placeholder.h"
#import "CommonUtils.h"


@interface NotesViewController ()
@property (weak, nonatomic) IBOutlet UITextView *tvNote;

@end

@implementation NotesViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

- (void) initView {
    
    self.tvNote.placeholder = @"Type notes here.";
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitAction:(id)sender {
    
    if (_tvNote.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_NOTE positive:ALERT_OK negative:nil];
        return;
    }
    
    NOTE = _tvNote.text;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    return 240.0f;
}


@end
