//
//  HomeViewController.m
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "HomeViewController.h"
#import "CustomerListViewController.h"
#import "UserEntity.h"
#import "CommonUtils.h"
#import "CustomerEntity.h"

@interface HomeViewController () {
    
    UserEntity *_user;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationItem.hidesBackButton = YES;
    
//    self.navigationItem.backBarButtonItem = nil;
    
    // global orderlist variable init.
    ORDERLIST = [[NSMutableArray alloc] init];
    CUSTOMER_LIST = [[NSMutableArray alloc] init];
    CUSTOMER_MENU = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
//    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Home";
//    self.navigationItem.backBarButtonItem = nil;
    _from = 0;
    
    [self getCustomerList];
    
}

- (void) viewDidAppear:(BOOL) animated {
    
    [super viewDidAppear: animated];
    
    [self.navigationItem setHidesBackButton:true animated:true];

}

- (IBAction)logoutAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)customerAction:(id)sender {
    
    CustomerListViewController *destVC = (CustomerListViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"CustomerListViewController"];
    
    _from = FROM_HOME;
    
    [self.navigationController pushViewController:destVC animated:YES];
        
}

- (void) getCustomerList {
    
    [self showLoadingView];
    
    NSString *url;
    
    if (_user._permission == ADMIN_PERMISSION) {
        
        url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GET_ALL_CUSTOMER];
        
    } else {
        
        url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GET_CUSTOMER, _user._idx];
    }
    
    NSLog(@"get customer url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"get customer url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSArray *arrItems = [responseObject objectForKey:RES_CUSTOMER_INFOS];
            
            [CUSTOMER_LIST removeAllObjects];
            [CUSTOMER_MENU removeAllObjects];
            
            if (arrItems.count > 0) {
                
                for (NSDictionary *dict in arrItems) {
                    
                    CustomerEntity *_customer = [[CustomerEntity alloc] init];
                    
                    _customer._idx = [[dict valueForKey:RES_IDX] intValue];
                    _customer._company = [dict valueForKey:COMPANY];
                    _customer._contact = [dict valueForKey:CONTACT];
                    _customer._address = [dict valueForKey:ADDRESS];
                    _customer._city = [dict valueForKey:CITY];
                    _customer._phone1 = [dict valueForKey:PHONE1];
                    _customer._phone2 = [dict valueForKey:PHONE2];
                    _customer._email = [dict valueForKey:EMAIL];
                    
                    [CUSTOMER_LIST addObject:_customer];
                    [CUSTOMER_MENU addObject:_customer._company];
                }
                
            } else {
                
                //[[JLToast makeText:@"No customer found" duration:2] show];
                [self.navigationController.view makeToast:@"No customer found"];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
    
    
}

@end
