//
//  InvoiceViewController.h
//  NuLook
//
//  Created by AOC on 27/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface InvoiceViewController : CommonViewController

@property (nonatomic, strong) NSString *_selectedCustomer;

@end
