//
//  CustomerMngViewController.h
//  NuLook
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CustomerEntity.h"

@interface CustomerMngViewController : BaseViewController

@property (nonatomic, strong) CustomerEntity * _selectedCustomer;

@end
