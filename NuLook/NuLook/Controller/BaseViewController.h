//
//  BaseViewController.h
//  NuLook
//
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController {
    
    BOOL _isConnecting;
}

@property (nonatomic) float keyboardHeight;

- (void) showLoadingView;
- (void) showLoadingViewWithTitle:(NSString *) title;
- (void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay ;
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative;
- (CGFloat) getOffsetYWhenShowKeybarod;

- (CGFloat) getKeyboardHeight;

@end