//
//  CustomerListViewController.m
//  NuLook
//
//

#import "CustomerListViewController.h"
#import "CustomerMngViewController.h"
#import "SalesViewController.h"
#import "CustomerCell.h"
#import "CommonUtils.h"
#import "CustomerEntity.h"


@interface CustomerListViewController () {
    
    UserEntity *_user;
    CustomerEntity *_selCustomer;
    
    NSMutableArray *_customerList;
}

@property (weak, nonatomic) IBOutlet UITableView *tbCustomer;

@end

@implementation CustomerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    _customerList = [[NSMutableArray alloc] init];
    
    [self getCustomer];
//    NSLog(@"%@", _user._username);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - get customer

- (void) getCustomer {
    
    [self showLoadingView];
    
    NSString *url;
    
    if (_user._permission == ADMIN_PERMISSION) {
        
        url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GET_ALL_CUSTOMER];
    
    } else {
        
        url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GET_CUSTOMER, _user._idx];
    }
    
    NSLog(@"get customer url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"get customer url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSArray *arrItems = [responseObject objectForKey:RES_CUSTOMER_INFOS];
            
            [_customerList removeAllObjects];
            
            if (arrItems.count > 0) {
                
                for (NSDictionary *dict in arrItems) {
                    
                    CustomerEntity *_customer = [[CustomerEntity alloc] init];
                    
                    _customer._idx = [[dict valueForKey:RES_IDX] intValue];
                    _customer._company = [dict valueForKey:COMPANY];
                    _customer._contact = [dict valueForKey:CONTACT];
                    _customer._address = [dict valueForKey:ADDRESS];
                    _customer._city = [dict valueForKey:CITY];
                    _customer._phone1 = [dict valueForKey:PHONE1];
                    _customer._phone2 = [dict valueForKey:PHONE2];
                    _customer._email = [dict valueForKey:EMAIL];
                    
                    [_customerList addObject:_customer];
                }
                
                [self.tbCustomer reloadData];
                
            } else {
                
                [self.navigationController.view makeToast:@"No customer found"];
                
                //[[Toast makeText:@"No customer found" duration:2] show];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}

#pragma mark - table view delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return _customerList.count;
    //for test
//    return 12;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomerCell *cell = (CustomerCell *) [tableView dequeueReusableCellWithIdentifier:@"CustomerCell"];
    
    //for test
    
//    NSString *customerName = [NSString stringWithFormat:@"Customer - %ld", (long)indexPath.row];
//    
//    [cell setCustomerName:customerName];
    
    CustomerEntity *selectedCustomer = _customerList[(int)indexPath.row];
    
    [cell setCustomerName:selectedCustomer._company];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _selCustomer = _customerList[indexPath.row];
   
    NSIndexPath *selectedIndexPath = [self.tbCustomer indexPathForSelectedRow];
    
    if (_from == FROM_SALE) {
        
        CustomerMngViewController *CustVC = (CustomerMngViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"CustomerMngViewController"];
        
        CustVC._selectedCustomer = _customerList[selectedIndexPath.row];
        
        [self.navigationController pushViewController:CustVC animated:YES];
        
    } else {
        
        SalesViewController * saleVC = (SalesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SalesViewController"];
        saleVC._selectedCustomer = _customerList[selectedIndexPath.row];
        [self.navigationController pushViewController:saleVC animated:YES];
    }
}

@end
