//
//  QRScanViewController.m
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "QRScanViewController.h"
#import "QTYViewController.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"
#import "Const.h"

@interface QRScanViewController ()
@property (weak, nonatomic) IBOutlet UIView *vCover;
@property (weak, nonatomic) IBOutlet UITextField *tfStyle;
@property (weak, nonatomic) IBOutlet UITextField *tfColor;
@property (weak, nonatomic) IBOutlet UITextField *tfPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfArrDate;
@property (weak, nonatomic) IBOutlet UITextField *tfPPK;

@end

@implementation QRScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initExternVars];
    
    [self initView];
    
    [self QRScannerView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initExternVars {
    
    S = @"";
    COLOR = @"";
    PPK_NO = 0;
    PRICE = 0.0;
}

- (void) initView {
    
    [self.navigationController setNavigationBarHidden:YES];
    
    self.tfStyle.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfColor.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfPrice.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfPPK.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (IBAction)backAction:(id)sender {
    
    BACK_WHERE = FROM_BACK;
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)confirmAction:(id)sender {
    
    S = self.tfStyle.text ;                     // S
    COLOR = self.tfColor.text;                  // C
    PRICE = [self.tfPrice.text floatValue];     // P
    PPK_NO = [self.tfPPK.text intValue];        // K
    
    BACK_WHERE = FROM_CONFIRM;
    
    //[self.navigationController popViewControllerAnimated:YES];
}

//- (IBAction)scanAction:(id)sender {

- (void) QRScannerView {
    
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        
        [self showAlertDialog:@"Error" message:@"Reader not suppported by the current device" positive:@"OK" negative:nil];

    }
}

#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [reader stopScanning];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [self.vCover setHidden:YES];
        
        NSArray *strArr = [result componentsSeparatedByString:@"\n"];
        
        if (strArr.count != 4) {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Error"
                                         message:@"Invalid QR code!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            [self okAction];
                                        }];

            
            [alert addAction:okButton];
       
            
            [self presentViewController:alert animated:YES completion:nil];

            
            return ;
        }
        
        S = [strArr[0] componentsSeparatedByString:@":"][1] ;                  // S
        COLOR = [strArr[1] componentsSeparatedByString:@":"][1];                // C
        PRICE = [[strArr[2] componentsSeparatedByString:@":"][1] floatValue];   // P
        PPK_NO = [[strArr[3] componentsSeparatedByString:@":"][2] intValue];    // K
        
        //NSLog(@"S:%d, C:%@, P:%f, K:%d", S, COLOR, PRICE, PPK_NO);
        
        self.tfStyle.text = S;
        self.tfColor.text = COLOR;
        self.tfPrice.text = [NSString stringWithFormat:@"%.2f", PRICE];
        self.tfPPK.text = [NSString stringWithFormat:@"%d", PPK_NO];
        
        [self gotoQuantity];

    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) gotoQuantity {
    
    QTYViewController *qtyVC = (QTYViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"QTYViewController"];

    [self.navigationController pushViewController:qtyVC animated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) okAction {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
