//
//  SalesViewController.m
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SalesViewController.h"
#import "QTYViewController.h"
#import "CustomerListViewController.h"
#import "InvoiceViewController.h"
#import "XCMultiSortTableView.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>
#import "UserEntity.h"
#import "CommonUtils.h"
#import "CustomerEntity.h"

#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"

@interface SalesViewController () <XCMultiTableViewDataSource, XCMultiTableViewDelegate> {
    
//    NSMutableArray *customerList;
//    NSMutableArray *customerMenu;
    UserEntity *_user;
    CGFloat vOrderWidth;
    int from;
    XCMultiTableView *tableView;
    
    int selectedRowIndex;
    
}

@property (weak, nonatomic) IBOutlet UITextField *txfSelectedCustomer;
@property (weak, nonatomic) IBOutlet UITextField *txfStyleCode;
@property (weak, nonatomic) IBOutlet UITextField *tfSubTtl;
@property (weak, nonatomic) IBOutlet UITextField *tfTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIView *vOrders;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subTtlBoxWidthConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfTtlBoxWidthConstrain;

@end

@implementation SalesViewController {
    
    int _colCount;
    
    NSMutableArray *headData;
    NSMutableArray *leftTableData;
    NSMutableArray *rightTableData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    //[self initView];
    
    tableView.leftHeaderWidth = 20.0f;
    
    tableView = [[XCMultiTableView alloc] initWithFrame:CGRectInset(self.vOrders.bounds, 0.0f, 0.0f)];
    tableView.leftHeaderEnable = YES;
    //    tableView.leftHeaderEnable = NO;

    [self.vOrders addSubview:tableView];

//    
//    [self getCustomer];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];

}

- (void) viewWillAppear:(BOOL)animated {
    
    selectedRowIndex = -1;
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    [self initView];
    
    vOrderWidth = self.view.frame.size.width;
    
    NSLog(@"vorder_width %f", vOrderWidth);
    
    if (BACK_WHERE == FROM_BACK) {
        
        [self initVars];
    }
    
    // if QR code was scaned
    NSLog(@"S on sale :  %@", S);
    
    //if (S !=0 ) self.txfStyleCode.text = [NSString stringWithFormat:@"%d", S];
    self.txfStyleCode.text = @"";
    
    NSString *model = [[UIDevice currentDevice] model];
    NSLog(@"device model : %@", model);
    
    if ([model isEqualToString:@"iPhone"]) deviceType = IPHONE;
    else {
        
        deviceType = IPAD;
        _subTtlBoxWidthConstrain.constant = 180.0f;
        _tfTtlBoxWidthConstrain.constant = 180.0f;
//        _tfSubTtl.frame.size.width = 1000;
        
    }
    
   
}

- (void) initVars {
    
    S = @"";
    COLOR = @"";
    PPK_NO = 0;
    PRICE = 0.0;
    
    selectedRowIndex = -1;
    
    _user = APPDELEGATE.Me;
    
//    customerList = [[NSMutableArray alloc] init];
//    customerMenu = [[NSMutableArray alloc] init];
    // for test
    //customerList = [[NSMutableArray alloc] initWithObjects:@"customer 1", @"cusotmer 2", @"customer 3", @"customer 4", @"customer 5",@"customer 6",nil];
    
    
    
}

- (void) initView {
    
    if (self._selectedCustomer) {
        _txfSelectedCustomer.text = self._selectedCustomer._company;
    } else {
        _txfSelectedCustomer.text = @"";
    }
    
    self.txfSelectedCustomer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.txfStyleCode.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfSubTtl.layer.borderColor = [[UIColor lightGrayColor] CGColor];    
    self.tfTotal.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self initData];
    
    tableView.datasource = self;
    tableView.delegate = self;
    [tableView reloadData];
}

- (CGFloat)getOffsetYWhenShowKeybarod {
    
    if ([self.txfStyleCode isFirstResponder]) {
        
        if (deviceType == IPHONE)
        return 230.0f;
        else return 300.0f;
    }
    
    return 50.0f;
}


#pragma mark - get customer information

/*
- (void) getCustomer {
    
    [self showLoadingView];
    
    NSString *url;
    
    if (_user._permission == ADMIN_PERMISSION) {
        
        url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GET_ALL_CUSTOMER];
        
    } else {
        
        url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GET_CUSTOMER, _user._idx];
    }
    
    NSLog(@"get customer url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"get customer url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSArray *arrItems = [responseObject objectForKey:RES_CUSTOMER_INFOS];
            
            [customerList removeAllObjects];
            
            if (arrItems.count > 0) {
                
                for (NSDictionary *dict in arrItems) {
                    
                    CustomerEntity *_customer = [[CustomerEntity alloc] init];
                    
                    _customer._idx = [[dict valueForKey:RES_IDX] intValue];
                    _customer._company = [dict valueForKey:COMPANY];
                    _customer._contact = [dict valueForKey:CONTACT];
                    _customer._address = [dict valueForKey:ADDRESS];
                    _customer._city = [dict valueForKey:CITY];
                    _customer._phone1 = [dict valueForKey:PHONE1];
                    _customer._phone2 = [dict valueForKey:PHONE2];
                    _customer._email = [dict valueForKey:EMAIL];
                    
                    [customerList addObject:_customer];
                    [customerMenu addObject:_customer._company];
                }
                
            } else {
                
                [[JLToast makeText:@"No customer found" duration:2] show];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}
*/
#pragma mark - button actions

- (IBAction)editCustomerAction:(id)sender {
    
    CustomerListViewController * destVC = (CustomerListViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"CustomerListViewController"];
    _from = FROM_SALE;
    
    [self.navigationController pushViewController:destVC animated:YES];    
    
}

- (IBAction)selectAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Customer" rows:CUSTOMER_MENU initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.txfSelectedCustomer setText:selectedValue];
        self._selectedCustomer = CUSTOMER_LIST[selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnSelect];
}

- (IBAction)cancelAction:(id)sender {
    
    //[self showAlertDialog:ALERT_TITLE message:CONFIRM_CANCEL positive:ALERT_OK negative:ALERT_CANCEL];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:ALERT_TITLE
                                 message:CONFIRM_CANCEL
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:ALERT_OK
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    //clear all table view values
                                    // table view
                                    
                                    [ORDERLIST removeAllObjects];
                                    [self initData];
                                    [tableView reloadData];
//                                    [self.navigationController popViewControllerAnimated:YES];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:ALERT_CANCEL
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                                   NSLog(@"cancel");
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)gotoHomeAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)printAction:(id)sender {
    
    NSLog(@"Go to Wifi print");
}


- (IBAction)addAction:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self isValidStyle]) {
        
        S = _txfStyleCode.text;
        COLOR = @"";
        PPK_NO = 0;
        PRICE = 0.0;
        
        [self gotoQuantity];
    }
}

- (IBAction)removeItemAction:(id)sender {
    
    NSLog(@"Remove selected item : %d", selectedRowIndex);
    
    if (selectedRowIndex == -1) {
        
        [self showAlertDialog:ALERT_TITLE message:SELECT_ITEM positive:ALERT_OK negative:nil];
        return;
    }
    
    [ORDERLIST removeObjectAtIndex:selectedRowIndex];
    [self initData];
    [tableView reloadData];
}

- (IBAction)invoiceAction:(id)sender {
    
    InvoiceViewController *invoiceVC = (InvoiceViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"InvoiceViewController"];
    
    invoiceVC._selectedCustomer = self.txfSelectedCustomer.text;
    [self.navigationController pushViewController:invoiceVC animated:YES];
    
}


- (IBAction)submitAction:(id)sender {
    
   
    if(_txfSelectedCustomer.text.length == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:INVALID_CUSTOMER delegate:self cancelButtonTitle:ALERT_OK otherButtonTitles:nil, nil];
        [alert show];
        return ;
    }
//    else if(_txfStyleCode.text.length == 0) {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:INVALID_STYLECODE delegate:self cancelButtonTitle:ALERT_OK otherButtonTitles:nil, nil];
//        [alert show];
//        return ;
//    }
   
    [self showLoadingViewWithTitle:nil];
    //NSString * _note = [NOTE encodeString:NSUTF8StringEncoding];
    if (NOTE.length == 0) NOTE = @"%20";
    
    //NSString *_customer_name = [self.txfSelectedCustomer.text encodeString:NSUTF8StringEncoding];
    
//    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@/%@", SERVER_URL, REQ_SUBMIT_ORDER, _tfSubTtl.text, _note, _customer_name];
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_SUBMIT_ORDER];
    
    NSMutableArray * orders;
    
    orders = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < ORDERLIST.count; i++) {
   
        OrderEntity *_order = (OrderEntity *) ORDERLIST[i];
        
        NSDictionary *orderCell = @{
                                    PARAM_STYLE :        _order._style ,
                                    PARAM_CUSTOMER :     [_txfSelectedCustomer.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                    PARAM_COLOR :        _order._color,
                                    PARAM_DESCRIPTION :  _order._description,
                                    PARAM_S :            [NSString stringWithFormat:@"%d", _order._s],
                                    PARAM_M :            [NSString stringWithFormat:@"%d",_order._m],
                                    PARAM_L :            [NSString stringWithFormat:@"%d",_order._l],
                                    PARAM_XL :           [NSString stringWithFormat:@"%d",_order._xl],
                                    PARAM_XL1 :          [NSString stringWithFormat:@"%d",_order._1xl],
                                    PARAM_XL2 :          [NSString stringWithFormat:@"%d",_order._2xl],
                                    PARAM_XL3 :          [NSString stringWithFormat:@"%d",_order._3xl],
                                    PARAM_QTY :          [NSString stringWithFormat:@"%d",_order._qty],
                                    PARAM_PRICE :        [NSString stringWithFormat:@"%.2f",_order._price],
                                    PARAM_DISC :         [NSString stringWithFormat:@"%.2f%@",_order._disc, @"%"],
                                    PARAM_TOTAL :        [NSString stringWithFormat:@"%d",_order._total]
                                    };
        
        [orders addObject:orderCell];
        
    }
    
    NSDictionary * paramOrderList = @{PARAM_ORDER_LIST: orders,
                                      PARAM_TOTAL:[_tfSubTtl.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                      PARAM_NOTE : NOTE,
                                      PARAM_CUSTOMER : [_txfSelectedCustomer.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]};
        
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];                                             // text/html request
    manager.requestSerializer = [AFJSONRequestSerializer serializer];                                               //json request
//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"]; // row request
    
    [manager POST:url parameters:paramOrderList headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        [self hideLoadingView];
        
        NSLog(@"add customer respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            //[[JLToast makeText:@"Order was was submitted successfully" duration:2] show];
            [self.navigationController.view makeToast:@"Order was submitted successfully!"];
            [ORDERLIST removeAllObjects];
            self.txfSelectedCustomer.text = @"";
            NOTE = @"";
            
            [self initData];
            [tableView reloadData];
            
        } else if (nResultCode == CODE_USERNAME_EXIST){
            
            [self showAlertDialog:nil message:@"submit failed" positive:ALERT_OK negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}


- (BOOL) isValidStyle {
    
    if(_txfStyleCode.text.length == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:INVALID_STYLECODE delegate:self cancelButtonTitle:ALERT_OK otherButtonTitles:nil, nil];
        
        [alert show];
        
        return NO;
    }
    return YES;
}

- (void) gotoQuantity {
    
    QTYViewController *qtyVC = (QTYViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"QTYViewController"];
    
    qtyVC.styleCode = [_txfStyleCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];;
    [self.navigationController pushViewController:qtyVC animated:YES];
    
}

- (void)initData {
    
    headData = [NSMutableArray arrayWithCapacity:15];
    
    [headData addObject:@"Style"];
    [headData addObject:@"Colour"];
    //[headData addObject:@"Description"];
    [headData addObject:@"S"];
    [headData addObject:@"M"];
    [headData addObject:@"L"];
    [headData addObject:@"XL"];
    [headData addObject:@"1XL"];
    [headData addObject:@"2XL"];
    [headData addObject:@"3XL"];
    [headData addObject:@"Qty"];
    [headData addObject:@"Price"];
    //[headData addObject:@"Disc"];
    [headData addObject:@"Total"];
    
    _colCount = (int)headData.count;
    
    //NSLog(@"headData count = %d", _colCount);
    
    leftTableData = [NSMutableArray arrayWithCapacity:ORDERLIST.count];
    
    NSMutableArray *two = [NSMutableArray arrayWithCapacity:ORDERLIST.count];
    
    for (int i = 0; i < ORDERLIST.count; i++) {
        
        [two addObject:[NSString stringWithFormat:@"%d", i + 1]];

    }
    
    [leftTableData addObject:two];    
    
    rightTableData = [NSMutableArray arrayWithCapacity:_colCount * ORDERLIST.count];
    
    NSMutableArray *twoR = [NSMutableArray arrayWithCapacity:_colCount * 2];
    
    int subTotal = 0;

    for (int i = 0; i < ORDERLIST.count; i++) {
        
        OrderEntity *_order = ORDERLIST[i];
        
        NSMutableArray *ary = [NSMutableArray arrayWithCapacity:_colCount * 2];
        
        for (int j = 0; j < _colCount; j++) {
            
            if (j == 0) [ary addObject:_order._style];
            else if (j == 1) [ary addObject:_order._color];
            else if (j == 2) [ary addObject:[NSString stringWithFormat:@"%d", _order._s]];
            else if (j == 3) [ary addObject:[NSString stringWithFormat:@"%d", _order._m]];
            else if (j == 4) [ary addObject:[NSString stringWithFormat:@"%d", _order._l]];
            else if (j == 5) [ary addObject:[NSString stringWithFormat:@"%d", _order._xl]];
            else if (j == 6) [ary addObject:[NSString stringWithFormat:@"%d", _order._1xl]];
            else if (j == 7) [ary addObject:[NSString stringWithFormat:@"%d", _order._2xl]];
            else if (j == 8) [ary addObject:[NSString stringWithFormat:@"%d", _order._3xl]];
            else if (j == 9) [ary addObject:[NSString stringWithFormat:@"%d", _order._qty]];
            else if (j == 10) [ary addObject:[NSString stringWithFormat:@"%0.2f", _order._price]];
            else if (j == 11) [ary addObject:[NSString stringWithFormat:@"%d", _order._total]];
        }
        
        [twoR addObject:ary];
        subTotal += _order._total;
    }
    
    [rightTableData addObject:twoR];
    _tfSubTtl.text = [NSString stringWithFormat:@"%d", subTotal];
    
    NSLog(@"order count : %d", (int)ORDERLIST.count);
}

#pragma mark - XCMultiTableViewDataSource

- (NSArray *)arrayDataForTopHeaderInTableView:(XCMultiTableView *)tableView {
    return [headData copy];
}


- (NSArray *)arrayDataForLeftHeaderInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [leftTableData objectAtIndex:section];
}

- (NSArray *)arrayDataForContentInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [rightTableData objectAtIndex:section];
}


- (NSUInteger)numberOfSectionsInTableView:(XCMultiTableView *)tableView {
    return [leftTableData count];
}

- (AlignHorizontalPosition)tableView:(XCMultiTableView *)tableView inColumn:(NSInteger)column {

//    if (column == 0) {
//        return AlignHorizontalPositionCenter;
//    }else if (column == 1) {
//        return AlignHorizontalPositionRight;
//    }
//    return AlignHorizontalPositionLeft;
    
    return AlignHorizontalPositionCenter;
}

- (CGFloat)tableView:(XCMultiTableView *)tableView contentTableCellWidth:(NSUInteger)column {
    
    if (column == 0 || column == _colCount - 1 || column == _colCount - 2) {
        
        return 80.0f;
    }
    if (column == 1) return 150;
    //vOrderWidth = self.view.frame.size.width - 33;
    //// view width - No cel width - 2 * 30 cell
    //CGFloat wid_th = (vOrderWidth - (2 * 30) - 50)/(_colCount - 3);

    //return wid_th;
    
    return 50.0f;

}

- (CGFloat)tableView:(XCMultiTableView *)tableView cellHeightInRow:(NSUInteger)row InSection:(NSUInteger)section {
    
    return 30.0f;
//    if (section == 0) {
//        return 60.0f;
//    }else {
//        return 30.0f;
//    }
}

//- (CGFloat) topHeaderHeightInTableView:(XCMultiTableView *)tableView {
//    
//    return 30.0f;
//}

- (UIColor *)tableView:(XCMultiTableView *)tableView bgColorInSection:(NSUInteger)section InRow:(NSUInteger)row InColumn:(NSUInteger)column {
    if (row == selectedRowIndex && section == 0) {
        return [UIColor redColor];
    }
    return [UIColor clearColor];
}

- (UIColor *)tableView:(XCMultiTableView *)tableView headerBgColorInColumn:(NSUInteger)column {
//    if (column == -1) {
//        return [UIColor redColor];
//    }else if (column == 1) {
//        return [UIColor grayColor];
//    }
//    return [UIColor clearColor];
    
    return [UIColor lightGrayColor];
}

- (NSString *)vertexName {
    return @"N";
}



#pragma mark - XCMultiTableViewDelegate

- (void)tableViewWithType:(MultiTableViewType)tableViewType didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tableViewType:%@, selectedIndexPath: %@", @(tableViewType), indexPath);
    
    selectedRowIndex = (int)indexPath.row;

    [tableView reloadData];
}

#pragma mark - TextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.txfStyleCode) {
        
        [self.tfSubTtl becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - QR Scanning part

- (IBAction)scanItemAction:(id)sender {
    
    [self QRScannerView];
}


- (void) QRScannerView {
    
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        
        [self showAlertDialog:@"Error" message:@"Reader not suppported by the current device" positive:@"OK" negative:nil];
        
    }
}

#pragma mark - QRCodeReader Delegate Methods


- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [reader stopScanning];
    
    [self dismissViewControllerAnimated:YES completion:^{
       
        
        NSArray *strArr = [result componentsSeparatedByString:@"\n"];
        
        if (strArr.count != 4) {
            
            [self showAlertDialog:@"Error" message:@"Invalid Code" positive:@"OK" negative:nil];
            
            return ;
        }
        
        S = [strArr[0] componentsSeparatedByString:@":"][1] ;         // S
        COLOR = [strArr[1] componentsSeparatedByString:@":"][1];                // C
        PRICE = [[strArr[2] componentsSeparatedByString:@":"][1] floatValue];   // P
        PPK_NO = [[strArr[3] componentsSeparatedByString:@":"][2] intValue];    // K
        
        
        NSLog(@"S:%@, C:%@, P:%f, K:%d", S, COLOR, PRICE, PPK_NO);
        
        [self gotoQuantityAfterScan];
        
    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) gotoQuantityAfterScan {
    
    QTYViewController *qtyVC = (QTYViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"QTYViewController"];
    
    qtyVC.styleCode = S;
    
    [self.navigationController pushViewController:qtyVC animated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



@end
