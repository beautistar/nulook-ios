//
//  LoginViewController.m
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"


@interface LoginViewController () {
    
    UserEntity *_user;
}

@property (weak, nonatomic) IBOutlet UITextField *txfUserName;
@property (weak, nonatomic) IBOutlet UITextField *txfPassword;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBar];
    self.navigationItem.hidesBackButton = YES;
    _user = APPDELEGATE.Me;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    BOOL isLoggedIn = [CommonUtils getUserLogin];
    
    if(isLoggedIn) {
        
        [self gotoHome];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.txfUserName.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.txfPassword.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.navigationItem.hidesBackButton = YES;
}

- (void) setNavigationBar {
    
    if (@available(iOS 13.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithOpaqueBackground];
        appearance.backgroundColor = [[UIColor alloc] initWithRed:2.0/255.0 green:136.0/255 blue:209/255.0 alpha:1];
        appearance.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
        self.navigationController.navigationBar.standardAppearance = appearance;
        self.navigationController.navigationBar.scrollEdgeAppearance = appearance;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    } else {
        // Fallback on earlier versions
    }
}
#pragma mark - TextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.txfUserName) {
        
        [self.txfPassword becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    
    return 210;
}

#pragma mark - login API

- (BOOL) isValid {
    
    if (_txfUserName.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_USERNAME positive:ALERT_OK negative:nil];
        return NO;
        
    } else if (_txfPassword.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:ALERT_OK negative:nil];
        
        return NO;
    }
    
    return YES;
}

- (IBAction)loginAction:(id)sender {
    
    if ([self isValid]) {
        
        [self doLogin];
    }
}

- (void) doLogin {
    
    [self showLoadingView];
    
    NSString *username = _txfUserName.text;
    NSString * password = _txfPassword.text;
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_LOGIN, username, password];
    
    NSLog(@"login request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"login response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:RES_IDX] intValue];
            _user._username = [responseObject valueForKey:RES_USERNAME];
            _user._password = password;
            _user._permission = [[responseObject valueForKey:RES_PERMISSION] intValue];
            
            [CommonUtils saveUserInfo];
            
            [CommonUtils setUserLogin:YES];
            
            [self gotoHome];
            
        } else if (result_code == CODE_UNREGISTER_USERANEM) {
            
            [self showAlertDialog:nil message:RES_UNREGISTERED_USERNAME positive:ALERT_OK negative:nil];
            
        } else if (result_code == CODE_WRONG_PWD) {
            
            [self showAlertDialog:nil message:CHECK_PWD positive:ALERT_OK negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];

}

- (void) gotoHome {
    
    HomeViewController *HomeVC = (HomeViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    self.navigationController.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:HomeVC animated:YES];
}

@end
