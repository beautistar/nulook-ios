//
//  QTYViewController.h
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

//#import "CommonViewController.h"


@interface QTYViewController : BaseViewController <UITextViewDelegate>
//@interface QTYViewController : CommonViewController <UITextViewDelegate>

@property (nonatomic, strong) NSString *styleCode;

@end
