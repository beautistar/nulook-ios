//
//  InvoiceViewController.m
//  NuLook
//
//  Created by AOC on 27/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "InvoiceViewController.h"
#import "XCMultiSortTableView.h"
#import "CommonUtils.h"
#import "OrderEntity.h"
#import "UITextView+Placeholder.h"

@interface InvoiceViewController () <XCMultiTableViewDataSource, XCMultiTableViewDelegate>  {
    
    int _colCount;
    float vOrderWidth;
    UserEntity *_user;
    
    float _height;
    
    NSMutableArray *headData;
    NSMutableArray *leftTableData;
    NSMutableArray *rightTableData;
}

@property (weak, nonatomic) IBOutlet UITextField *tfEmilTo;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTax13;
@property (weak, nonatomic) IBOutlet UILabel *lblTax13Value;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UITextField *tfTerm;
@property (weak, nonatomic) IBOutlet UIView *vBorder;
@property (weak, nonatomic) IBOutlet UIView *vOrders;
@property (weak, nonatomic) IBOutlet UITextView *tvRemark;
@property (weak, nonatomic) IBOutlet UILabel *tfInvoiceToCustomer;


@end

@implementation InvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView  {
    
    self.tvRemark.placeholder = @"Remarks:";
    
    self.tfEmilTo.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.lblSubTotal.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblSubTotalValue.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblTax13.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblTax13Value.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblDiscount.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblDiscountValue.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblTotal.layer.borderColor = [[UIColor redColor] CGColor];
    self.lblTotalValue.layer.borderColor = [[UIColor redColor] CGColor];
    self.tfTerm.layer.borderColor = [[UIColor redColor] CGColor];
    
    self.tfInvoiceToCustomer.text = self._selectedCustomer;
    
    self.vBorder.layer.borderColor = [[UIColor blackColor] CGColor];
    
    [self initData];
    
    XCMultiTableView *tableView = [[XCMultiTableView alloc] initWithFrame:CGRectInset(self.vOrders.bounds, 0.0f, 0.0f)];
    tableView.leftHeaderEnable = YES;
//    tableView.leftHeaderEnable = NO;
    tableView.datasource = self;
    tableView.delegate = self;
    [self.vOrders addSubview:tableView];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getHeight:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    NSString *model = [[UIDevice currentDevice] model];
    NSLog(@"device model : %@", model);
    
    if ([model isEqualToString:@"iPhone"]) deviceType = IPHONE;
    else deviceType = IPAD;
    
}

- (void) getHeight:(NSNotification *) notification {
    
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardFrame = [keyboardFrameBegin CGRectValue];
    _height = keyBoardFrame.size.height ;
}

- (void)initData {
    
    headData = [NSMutableArray arrayWithCapacity:12];
    
    [headData addObject:@"Style"];
    [headData addObject:@"Colour"];
    //[headData addObject:@"Description"];
    [headData addObject:@"S"];
    [headData addObject:@"M"];
    [headData addObject:@"L"];
    [headData addObject:@"XL"];
    [headData addObject:@"1X"];
    [headData addObject:@"2X"];
    [headData addObject:@"3X"];
    [headData addObject:@"Qty"];
    [headData addObject:@"Price"];
    //[headData addObject:@"Disc"];
    [headData addObject:@"Total"];
    
    _colCount = (int)headData.count;
    
    NSLog(@"headData count = %d", _colCount);
    
    leftTableData = [NSMutableArray arrayWithCapacity:ORDERLIST.count];
    
    NSMutableArray *two = [NSMutableArray arrayWithCapacity:ORDERLIST.count];
    
    for (int i = 0; i < ORDERLIST.count; i++) {
        
        [two addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    [leftTableData addObject:two];
    
    rightTableData = [NSMutableArray arrayWithCapacity:_colCount * ORDERLIST.count];
    
    NSMutableArray *twoR = [NSMutableArray arrayWithCapacity:_colCount];
    
    int subTotal = 0;
    
    for (int i = 0; i < ORDERLIST.count; i++) {
        
        OrderEntity *_order = ORDERLIST[i];
        
        NSMutableArray *ary = [NSMutableArray arrayWithCapacity:_colCount];
        
        for (int j = 0; j < _colCount; j++) {
            
            if (j == 0) [ary addObject:_order._style];
            else if (j == 1) [ary addObject:_order._color];
            else if (j == 2) [ary addObject:[NSString stringWithFormat:@"%d", _order._s]];
            else if (j == 3) [ary addObject:[NSString stringWithFormat:@"%d", _order._m]];
            else if (j == 4) [ary addObject:[NSString stringWithFormat:@"%d", _order._l]];
            else if (j == 5) [ary addObject:[NSString stringWithFormat:@"%d", _order._xl]];
            else if (j == 6) [ary addObject:[NSString stringWithFormat:@"%d", _order._1xl]];
            else if (j == 7) [ary addObject:[NSString stringWithFormat:@"%d", _order._2xl]];
            else if (j == 8) [ary addObject:[NSString stringWithFormat:@"%d", _order._3xl]];
            else if (j == 9) [ary addObject:[NSString stringWithFormat:@"%d", _order._qty]];
            else if (j == 10) [ary addObject:[NSString stringWithFormat:@"%0.2f", _order._price]];
            else if (j == 11) [ary addObject:[NSString stringWithFormat:@"%d", _order._total]];
            
        }
        [twoR addObject:ary];
        subTotal += _order._total;
    }
    
    [rightTableData addObject:twoR];
    _lblSubTotalValue.text = [NSString stringWithFormat:@"%d", subTotal];
    _tvRemark.text = NOTE;

}

#pragma mark - XCMultiTableViewDataSource


- (NSArray *)arrayDataForTopHeaderInTableView:(XCMultiTableView *)tableView {
    return [headData copy];
}


- (NSArray *)arrayDataForLeftHeaderInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [leftTableData objectAtIndex:section];
}

- (NSArray *)arrayDataForContentInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [rightTableData objectAtIndex:section];
}


- (NSUInteger)numberOfSectionsInTableView:(XCMultiTableView *)tableView {
    return [leftTableData count];
}

- (AlignHorizontalPosition)tableView:(XCMultiTableView *)tableView inColumn:(NSInteger)column {
    //    if (column == 0) {
    //        return AlignHorizontalPositionCenter;
    //    }else if (column == 1) {
    //        return AlignHorizontalPositionRight;
    //    }
    //    return AlignHorizontalPositionLeft;
    
    return AlignHorizontalPositionCenter;
}

- (CGFloat)tableView:(XCMultiTableView *)tableView contentTableCellWidth:(NSUInteger)column {
    
    if (deviceType == IPHONE) {
    
        if (column == 0 || column == _colCount - 1 || column == _colCount - 2) { //style & total & price field
            
            return 30.0f;
        }
        if (column == 1) return 40;// colour field
        vOrderWidth = self.view.frame.size.width - 33;
        
        // view width - No cel width - 2 * 30 cell
        CGFloat _width = (vOrderWidth - (3 * 30) - 40 - 20)/(_colCount - 4);
        
        return _width;
        
    } else {
    
        if (column == 0)  {// style
            
            return 90;
        }
        
        if (column == _colCount - 1) {//& Total
            
            return 50;
        }
        if (column == _colCount - 3) return 10;
        
        if (column ==  _colCount - 2) return 40; // price field
        if (column == 1) {// color cell
            
            return self.view.frame.size.width - 25 - 20 - 90 - 50 - 40 - 10 - (7 * 7); // total - margin(2 * 10) - leftTableViewCell Width 20
        }
        return 7;
    }
}


- (CGFloat)tableView:(XCMultiTableView *)tableView cellHeightInRow:(NSUInteger)row InSection:(NSUInteger)section {
    
    return 30.0f;
    //    if (section == 0) {
    //        return 60.0f;
    //    }else {
    //        return 30.0f;
    //    }
}

//- (CGFloat) topHeaderHeightInTableView:(XCMultiTableView *)tableView {
//
//    return 30.0f;
//}

- (UIColor *)tableView:(XCMultiTableView *)tableView bgColorInSection:(NSUInteger)section InRow:(NSUInteger)row InColumn:(NSUInteger)column {
    //    if (row == 1 && section == 0) {
    //        return [UIColor redColor];
    //    }
    return [UIColor clearColor];
}

- (UIColor *)tableView:(XCMultiTableView *)tableView headerBgColorInColumn:(NSUInteger)column {
    //    if (column == -1) {
    //        return [UIColor redColor];
    //    }else if (column == 1) {
    //        return [UIColor grayColor];
    //    }
    //    return [UIColor clearColor];
    
    return [UIColor lightGrayColor];
}

- (NSString *)vertexName {
    return @"N";
}



#pragma mark - XCMultiTableViewDelegate

- (void)tableViewWithType:(MultiTableViewType)tableViewType didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tableViewType:%@, selectedIndexPath: %@", @(tableViewType), indexPath);
}


- (IBAction)closeAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)emailToAction:(id)sender {
    
    if (_tfEmilTo.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_MAIL positive:ALERT_OK negative:nil];
        return;
    }
    
    [self showLoadingViewWithTitle:nil];
    
    NSString *toMail = [_tfEmilTo.text encodeString:NSUTF8StringEncoding];
    //NSString * _note = [NOTE encodeString:NSUTF8StringEncoding];
    //NSString * _customer_name = [_tfInvoiceToCustomer.text encodeString:NSUTF8StringEncoding];
    
    if (NOTE.length == 0) NOTE = @"%20";
    NSString *url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_SENDINVOICE, _user._idx, toMail];
    
    NSLog(@"%d", (int)ORDERLIST.count);
    
    NSMutableArray * orders;
    
    orders = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < ORDERLIST.count; i++) {
        
        OrderEntity *_order = (OrderEntity *) ORDERLIST[i];
        
        NSLog(@"order style : %@", _order._style);
        
        NSDictionary *orderCell = @{
                                    PARAM_STYLE :        _order._style ,
                                    //PARAM_COLOR :        [_order._color encodeString:NSUTF8StringEncoding],
                                    PARAM_COLOR :        _order._color,
                                    PARAM_DESCRIPTION :  _order._description,
                                    PARAM_S :            [NSString stringWithFormat:@"%d", _order._s],
                                    PARAM_M :            [NSString stringWithFormat:@"%d",_order._m],
                                    PARAM_L :            [NSString stringWithFormat:@"%d",_order._l],
                                    PARAM_XL :           [NSString stringWithFormat:@"%d",_order._xl],
                                    PARAM_XL1 :          [NSString stringWithFormat:@"%d",_order._1xl],
                                    PARAM_XL2 :          [NSString stringWithFormat:@"%d",_order._2xl],
                                    PARAM_XL3 :          [NSString stringWithFormat:@"%d",_order._3xl],
                                    PARAM_QTY :          [NSString stringWithFormat:@"%d",_order._qty],
                                    PARAM_PRICE :        [NSString stringWithFormat:@"%.2f",_order._price],
                                    PARAM_DISC :         [NSString stringWithFormat:@"%.2f%@",_order._disc, @"%"],
                                    PARAM_TOTAL :        [NSString stringWithFormat:@"%d",_order._total]
                                    };
        
        [orders addObject:orderCell];
        
    }
    
    NSDictionary * paramOrderList = @{PARAM_ORDER_LIST: orders,
                                      PARAM_TOTAL:[_lblSubTotalValue.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                      PARAM_NOTE : NOTE,
                                      PARAM_CUSTOMER : [_tfInvoiceToCustomer.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]};
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:paramOrderList headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        [self hideLoadingView];
        
        NSLog(@"send invoice respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            //[[JLToast makeText:@"successfully sent" duration:2] show];
            
            [self.navigationController.view makeToast:@"successfully sent"];
            
            
            NOTE = @"";
            _tvRemark.text = @"";
            _tfEmilTo.text = @"";
            
        } else if (nResultCode == CODE_USERNAME_EXIST){
            
            [self showAlertDialog:nil message:@"Failed to send." positive:ALERT_OK negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}


//
//- (CGFloat) getOffsetYWhenShowKeybarod {
//    
//    if([self.tfEmilTo isFirstResponder]) {
//        
//        if (deviceType == IPHONE)
//        return 250;
//        
//        else {
//            
//            return _height;
//            
//            //return -1 * [self getKeyboardHeight];
//            
//        }
//    }
//    
//    return 100;
//}

@end
