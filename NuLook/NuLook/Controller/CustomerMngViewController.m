//
//  CustomerMngViewController.m
//  NuLook
//
//

#import "CustomerMngViewController.h"
#import "CommonUtils.h"



@interface CustomerMngViewController () {
    
    UserEntity *_user;
    
    //constrains
    __weak IBOutlet NSLayoutConstraint *lblCompanyYConstrain;
    __weak IBOutlet NSLayoutConstraint *lblContractYConstrain;
    __weak IBOutlet NSLayoutConstraint *lblAddressYConstrain;
    __weak IBOutlet NSLayoutConstraint *lblCityYConstrain;
    __weak IBOutlet NSLayoutConstraint *lblPhone1YConstrain;
    __weak IBOutlet NSLayoutConstraint *lblPhone2YConstrain;
    __weak IBOutlet NSLayoutConstraint *lblEmailYConstrain;
    

}

@property (weak, nonatomic) IBOutlet UITextField *tfCompany;
@property (weak, nonatomic) IBOutlet UITextField *tfContact;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfCity;
@property (weak, nonatomic) IBOutlet UITextField *tfPhone1;
@property (weak, nonatomic) IBOutlet UITextField *tfPhone2;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@end


@implementation CustomerMngViewController

@synthesize _selectedCustomer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    [self initView];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSString *model = [[UIDevice currentDevice] model];
    NSLog(@"device model : %@", model);
    
    if ([model isEqualToString:@"iPhone"]) {
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            
            deviceType = IPHONE4;
            
            [self initVieWiPhone4];
            
        } else {
            deviceType = IPHONE;
        }
    }
    
    else {
        
        deviceType = IPAD;
    }
}

- (void) initView {
    
    self.navigationItem.hidesBackButton = true;
    self.tfCompany.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfContact.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfAddress.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfCity.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfPhone1.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfPhone2.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tfEmail.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    if (_selectedCustomer._idx != 0) {
        
        _tfCompany.text = _selectedCustomer._company;
        _tfContact.text = _selectedCustomer._contact;
        _tfAddress.text = _selectedCustomer._address;
        _tfCity.text = _selectedCustomer._city;
        _tfPhone1.text = _selectedCustomer._phone1;
        _tfPhone2.text = _selectedCustomer._phone2;
        _tfEmail.text = _selectedCustomer._email;
    }
}

- (void) initVieWiPhone4 {
    
    lblCompanyYConstrain.constant = 2;
    lblContractYConstrain.constant = 2;
    lblAddressYConstrain.constant = 2;
    lblCityYConstrain.constant = 2;
    lblPhone1YConstrain.constant = 2;
    lblPhone2YConstrain.constant = 2;
    lblEmailYConstrain.constant = 2;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)getOffsetYWhenShowKeybarod {
    
    if (deviceType == IPAD) {
        
        return 280;
    } else {

        if ([self.tfPhone1 isFirstResponder] || [self.tfPhone2 isFirstResponder]) {
            
            return 130.0f;
        } else if ([self.tfEmail isFirstResponder]) {
            
            return 60.0f;
        }

        return 240.0f;
    }
}


- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

#pragma mark - add customer API

- (IBAction)addAction:(id)sender {
    
    if([self isValid]) {
        
        [self addCustomer];
    }
}


- (BOOL) isValid {
    
    if (self.tfCompany.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_COMPANY positive:ALERT_OK negative:nil];
        return NO;
        
    } else if (self.tfPhone1.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_PHONE1 positive:ALERT_OK negative:nil];
        return NO;
    }
    
    return YES;
}


- (void) addCustomer {
    
    [self showLoadingViewWithTitle:nil];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_ADD_CUSTOMER];
    
    NSDictionary *params = @{
                             USER_IDX : [NSNumber numberWithInt:_user._idx],
                             COMPANY : [self.tfCompany.text encodeString:NSUTF8StringEncoding],
                             CONTACT : [self.tfContact.text encodeString:NSUTF8StringEncoding],
                             ADDRESS : [self.tfAddress.text encodeString:NSUTF8StringEncoding],
                             CITY : [self.tfCity.text encodeString:NSUTF8StringEncoding],
                             PHONE1 : [self.tfPhone1.text encodeString:NSUTF8StringEncoding],
                             PHONE2 : [self.tfPhone2.text encodeString:NSUTF8StringEncoding],
                             EMAIL : [self.tfEmail.text encodeString:NSUTF8StringEncoding]
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    //[manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        [self hideLoadingView];
        
        NSLog(@"add customer respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            //[[JLToast makeText:@"New customer was added successfully" duration:2] show];
            [self.navigationController.view makeToast:@"New customer was added successfully"];
        } else if (nResultCode == CODE_USERNAME_EXIST){
            
            [self showAlertDialog:nil message:EXIST_CUS_NAME positive:ALERT_OK negative:nil];
     
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}

- (IBAction)editAction:(id)sender {
    
    if ([self isValid]) {
        
        if (_selectedCustomer._idx == 0) {
            
            [self showAlertDialog:ALERT_TITLE message:INVALID_EDITCUSTOMER positive:ALERT_OK negative:nil];
            return;
            
        } else {
            
            [self editCustomer];
        }
    }
}

- (void) editCustomer {
    
    [self showLoadingViewWithTitle:nil];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_EDIT_CUSTOMER];
    
    NSLog(@"edit customer request url : %@", url);
    
    NSDictionary *params = @{
                             CUSTOMER_IDX : [NSNumber numberWithInt:_selectedCustomer._idx],
                             COMPANY : [self.tfCompany.text encodeString:NSUTF8StringEncoding],
                             CONTACT : [self.tfContact.text encodeString:NSUTF8StringEncoding],
                             ADDRESS : [self.tfAddress.text encodeString:NSUTF8StringEncoding],
                             CITY : [self.tfCity.text encodeString:NSUTF8StringEncoding],
                             PHONE1 : [self.tfPhone1.text encodeString:NSUTF8StringEncoding],
                             PHONE2 : [self.tfPhone2.text encodeString:NSUTF8StringEncoding],
                             EMAIL : [self.tfEmail.text encodeString:NSUTF8StringEncoding]
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"edit customer result : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            //[[JLToast makeText:@"Success!" duration:2] show];
            [self.navigationController.view makeToast:@"Success!"];
        } else {
            
            //[[JLToast makeText:@"Fail!" duration:2] show];
            [self.navigationController.view makeToast:@"Fail!"];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}

- (IBAction)deleteAction:(id)sender {
    
    if ([self isValid]) {
        
        if (_selectedCustomer._idx == 0) {
            
            [self showAlertDialog:ALERT_TITLE message:INVALID_DELCUSTOMER positive:ALERT_OK negative:nil];
            return;
            
        } else {
            
            [self deleteCustomer];
        }
    }
}

- (void) deleteCustomer {
    
    [self showLoadingViewWithTitle:nil];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_DEL_CUSTOMER, _selectedCustomer._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"delete response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            //[[JLToast makeText:@"Delete success" duration:2] show];
            [self.navigationController.view makeToast:@"Delete success"];
            
            [self gotoCustomerList];
            
        } else {
            
            //[JLToast makeText:@"Delete failed" duration:2];
            [self.navigationController.view makeToast:@"Delete failed"];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
}

- (void) gotoCustomerList {
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
