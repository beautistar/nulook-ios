//
//  QRScanViewController.h
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRCodeReaderDelegate.h"
#import "BaseViewController.h"
@interface QRScanViewController : BaseViewController <QRCodeReaderDelegate, UIAlertViewDelegate>

@end
