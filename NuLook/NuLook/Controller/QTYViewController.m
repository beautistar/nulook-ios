//
//  QTYViewController.m
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "QTYViewController.h"
#import "QRScanViewController.h"
#import "CommonUtils.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>
#import "UITextView+Placeholder.h"
#import "SalesViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@interface QTYViewController () {
    
    NSArray *ppkList;
    NSArray *colorList;
    int selectedPPK;
    
    CGFloat _height;
    
}
@property (weak, nonatomic) IBOutlet UITextField *tfStyle;
@property (weak, nonatomic) IBOutlet UITextField *tfPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfColor;
@property (weak, nonatomic) IBOutlet UITextField *tfS;
@property (weak, nonatomic) IBOutlet UITextField *tfM;
@property (weak, nonatomic) IBOutlet UITextField *tfL;
@property (weak, nonatomic) IBOutlet UITextField *tfXL3;
@property (weak, nonatomic) IBOutlet UITextField *tf1XL;
@property (weak, nonatomic) IBOutlet UITextField *tf2XL;
@property (weak, nonatomic) IBOutlet UITextField *tf3XL;
@property (weak, nonatomic) IBOutlet UITextField *tfPPKCount;
@property (weak, nonatomic) IBOutlet UITextField *tfPPK;
@property (weak, nonatomic) IBOutlet UITextField *tfDiscount;
@property (weak, nonatomic) IBOutlet UIView *vBorder;
@property (weak, nonatomic) IBOutlet UIButton *btnPPK;
@property (weak, nonatomic) IBOutlet UIButton *btnColorMenu;
@property (weak, nonatomic) IBOutlet UITextView *tvNote;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblStyleYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfStyleYConstrain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPriceYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblColorYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfPriceYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfColorYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblQtyYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vBorderYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vPPKYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vPPKCntYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblDiscYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfDiscYConstrain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vBorderHeightConstrain;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *XL1ViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *MViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *LViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *XLViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *XL2ViewYXonstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *XL3ViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ONESBtnViewYConstrain;


@end

@implementation QTYViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getHeight:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    if (COLOR.length != 0) {
        
        colorList = [COLOR componentsSeparatedByString:@","];
        
    } else {
        
        [self.btnColorMenu setHidden:YES];
        
    }
    
    NSString *model = [[UIDevice currentDevice] model];
    NSLog(@"device model : %@", model);
    
    if ([model isEqualToString:@"iPhone"]) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            
            deviceType = IPHONE4;
            [self initVieWiPhone4];
            
        } else {
            deviceType = IPHONE;
            _lblStyleYConstrain.constant = 10;
            _tfStyleYConstrain.constant = 10;
        }
    }
    else {
        deviceType = IPAD;
        
        [self initViewIPad];
        
    }
}

- (void) getHeight:(NSNotification *) notification {
    
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardFrame = [keyboardFrameBegin CGRectValue];
    _height = keyBoardFrame.size.height ;
    
    NSLog(@"keyboard height : %f", _height);
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification object:nil];
    
    
}

- (void) initViewIPad {
    
    _lblPriceYConstrain.constant = 30;
    _lblColorYConstrain.constant = 30;
    
    _lblQtyYConstrain.constant = 30;
    
    _tfPriceYConstrain.constant = 30;
    _tfColorYConstrain.constant = 30;
    
    _vBorderYConstrain.constant = 45;
    
    _vPPKYConstrain.constant = 50;
    _vPPKCntYConstrain.constant = 50;
    _lblDiscYConstrain.constant = 20;
    _tfDiscYConstrain.constant = 20;
    
    
    // quantity view y constrain
    _SViewYConstrain.constant = 20;
    _XL1ViewYConstrain.constant = 20;
    _vBorderHeightConstrain.constant = 250;
    _MViewYConstrain.constant = 20;
    _LViewYConstrain.constant = 20;
    _XLViewYConstrain.constant = 20;
    _XL2ViewYXonstrain.constant = 20;
    _XL3ViewYConstrain.constant = 20;
    _ONESBtnViewYConstrain.constant = 20;
    
}

- (void) initVieWiPhone4 {
    
    _lblStyleYConstrain.constant = 10;
    _tfStyleYConstrain.constant = 10;
    
    _lblPriceYConstrain.constant = 2;
    _lblColorYConstrain.constant = 2;
    
    _lblQtyYConstrain.constant = -10;
    
    _tfPriceYConstrain.constant = 2;
    _tfColorYConstrain.constant = 2;
    
    _vBorderYConstrain.constant = 5;
    
    _vPPKYConstrain.constant = 5;
    _vPPKCntYConstrain.constant = 5;
    _lblDiscYConstrain.constant = 2;
    _tfDiscYConstrain.constant = 2;
    
    
    // quamtity view y constrain
    _SViewYConstrain.constant = 0;
    _XL1ViewYConstrain.constant = 0;
    _vBorderHeightConstrain.constant = 155;
    _MViewYConstrain.constant = 5;
    _LViewYConstrain.constant = 5;
    _XLViewYConstrain.constant = 5;
    _XL2ViewYXonstrain.constant = 5;
    _XL3ViewYConstrain.constant = 5;
    _ONESBtnViewYConstrain.constant = 5;    
    
}

- (void) initVars {   

    
//    ppkList = [[NSMutableArray alloc] initWithObjects:@"1/S 2/M 2/L 1/1XL (8PCs)", @"1/S 2/M 2/L 2/1XL 1/2XL (10PCs)", @"2/1XL 2/2XL 1/3XL (5PCs)", @"2/S 2/M 2/L 2/XL 2/1XL(10PCs)", nil];
    ppkList = [[NSArray alloc] initWithObjects:@"PPK #1", @"PPK #2", @"PPK #3", @"PPK #4", nil];
    //colorList = [[NSArray alloc] initWithObjects:@"red", @"blue", @"green", nil];    
    
}

- (void) initView {
    
    self.navigationItem.hidesBackButton = true;
    
    for (int i = 81 ; i < 94 ; i++ ) {
        
        UITextField *tfds = (UITextField *) [self.view viewWithTag:i];
        tfds.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    
    self.vBorder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    self.tvNote.placeholder = @"Type note here.";
//    self.tvNote.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    // if QR code was scaned
    
    if (S.length != 0) {
        
        self.tfStyle.text = S;
        //self.tfColor.text = COLOR;
        if(PRICE > 0)
        self.tfPrice.text = [NSString stringWithFormat:@"%.2f", PRICE];
        else self.tfPrice.text = @"";
//        self.tfPPK.text = [NSString stringWithFormat:@"PPK #%d", PPK_NO];
        
//        switch (PPK_NO) {
//            
//            case 1:
//                [self setPPK_1:1];
//                break;
//            case 2:
//                [self setPPK_2:1];
//                break;
//            case 3:
//                [self setPPK_3:1];
//                break;
//            case 4:
//                [self setPPK_4:1];
//                break;
//        }
    } else {
        
        _tfStyle.text = self.styleCode;
    }
}

#pragma mark - button actions

- (IBAction)clearAction:(id)sender {    
    
    for (int i = 81 ; i < 94 ; i++ ) {
        
        UITextField *tfds = (UITextField *) [self.view viewWithTag:i];
        tfds.text = @"";
    }
    
    // clear scaned data
//    STYLE = 0;
//    COLOR = @"";
//    PRICE = 0.00;
//    PPK_NO = 0;
//    S = 0;
}

- (IBAction)nextAction:(id)sender {
    
    //save item and go to QR code scan for add another item.    
    
    QRScanViewController *destVC = (QRScanViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"QRScanViewController"];
    [self.navigationController pushViewController:destVC animated:YES];
    
}

- (IBAction)colorMenuAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Colour" rows:colorList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.tfColor setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnColorMenu];
    
}

- (IBAction)hotkeyAction:(id)sender {
    
    self.tfS.text = [NSString stringWithFormat:@"%d", 1];
    self.tfM.text = [NSString stringWithFormat:@"%d", 1];
    self.tfL.text = [NSString stringWithFormat:@"%d", 1];
    self.tfXL3.text = [NSString stringWithFormat:@"%d", 1];
    self.tf1XL.text = [NSString stringWithFormat:@"%d", 1];
    
}

- (IBAction)ppkDropAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Pre-Pack" rows:ppkList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        selectedPPK = (int)selectedIndex + 1;
        [self.tfPPK setText:selectedValue];
        self.tfPPKCount.text = [NSString stringWithFormat:@"%d", 1];
        
        switch (selectedPPK) {
            case 1:
                [self setPPK_1:1];
                break;
            case 2:
                [self setPPK_2:1];
                break;
            case 3:
                [self setPPK_3:1];
                break;
            case 4:
                [self setPPK_4:1];
                break;
        }
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnPPK];
    
}

- (IBAction)minusAction:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    switch (btn.tag) {
        case S_MIN:
            self.tfS.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tfS.text intValue]]];
            break;
            
        case M_MIN:
            self.tfM.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tfM.text intValue]]];
            break;
            
        case L_MIN:
            self.tfL.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tfL.text intValue]]];
            break;
            
        case XL3_MIN:
            self.tfXL3.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tfXL3.text intValue]]];
            break;
            
        case XL_MIN:
            self.tf1XL.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tf1XL.text intValue]]];
            break;
            
        case XXL_MIN:
            self.tf2XL.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tf2XL.text intValue]]];
            break;
            
        case XXXL_MIN:
            self.tf3XL.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tf3XL.text intValue]]];
            break;
            
        case PPK_MIN:
            self.tfPPKCount.text = [NSString stringWithFormat:@"%d", [self decrease:[self.tfPPKCount.text intValue]]];

            switch (selectedPPK) {
                    
                case 1:
                    [self setPPK_1:[self.tfPPKCount.text intValue]];
                    break;
                case 2:
                    [self setPPK_2:[self.tfPPKCount.text intValue]];
                    break;
                case 3:
                    [self setPPK_3:[self.tfPPKCount.text intValue]];
                    break;
                case 4:
                    [self setPPK_4:[self.tfPPKCount.text intValue]];
                    break;
            }
            
            break;
    }
}

- (IBAction)plusAction:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    switch (btn.tag) {
        case S_MAX:
            self.tfS.text = [NSString stringWithFormat:@"%d", [self increase:[self.tfS.text intValue]]];
            break;
            
        case M_MAX:
            self.tfM.text = [NSString stringWithFormat:@"%d", [self increase:[self.tfM.text intValue]]];
            break;
            
        case L_MAX:
            self.tfL.text = [NSString stringWithFormat:@"%d", [self increase:[self.tfL.text intValue]]];
            break;
            
        case XL3_MAX:
            self.tfXL3.text = [NSString stringWithFormat:@"%d", [self increase:[self.tfXL3.text intValue]]];
            break;
            
        case XL_MAX:
            self.tf1XL.text = [NSString stringWithFormat:@"%d", [self increase:[self.tf1XL.text intValue]]];
            break;
            
        case XXL_MAX:
            self.tf2XL.text = [NSString stringWithFormat:@"%d", [self increase:[self.tf2XL.text intValue]]];
            break;
            
        case XXXL_MAX:
            self.tf3XL.text = [NSString stringWithFormat:@"%d", [self increase:[self.tf3XL.text intValue]]];
            break;
            
        case PPK_MAX:
            self.tfPPKCount.text = [NSString stringWithFormat:@"%d", [self increase:[self.tfPPKCount.text intValue]]];
            
            switch (selectedPPK) {
                    
                case 1:
                    [self setPPK_1:[self.tfPPKCount.text intValue]];
                    break;
                case 2:
                    [self setPPK_2:[self.tfPPKCount.text intValue]];
                    break;
                case 3:
                    [self setPPK_3:[self.tfPPKCount.text intValue]];
                    break;
                case 4:
                    [self setPPK_4:[self.tfPPKCount.text intValue]];
                    break;
            }
            
            break;
    }
}

- (IBAction)backAction:(id)sender {
    
    NSMutableArray *tmpVCA = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    int index = 2; // (int)tmpVCA.count - 2;// self = count - 1, login, home, sale
//    int index = 3; // (int)tmpVCA.count - 2;// self = count - 1, login, home, customerlist, sale
    id salesVC;
    
//    if (_from == FROM_HOME) {
//        
//        salesVC = [tmpVCA objectAtIndex: 3];
//        
//        
//    } else {
//        
//        salesVC = [tmpVCA objectAtIndex:2];
//    }
//    
//    if ([salesVC isKindOfClass:[SalesViewController class]]) {
//        [self.navigationController popToViewController:salesVC animated:true];
//    }
    
    for (int i = 0 ; i < tmpVCA.count ; i++) {
        
        salesVC = [tmpVCA objectAtIndex:i];
        
        if ([salesVC isKindOfClass:[SalesViewController class]]) {
            [self.navigationController popToViewController:salesVC animated:true];
        }
    }
}


- (IBAction)addAction:(id)sender {
    
    if ([self isValid]) {
    
        OrderEntity *order = [[OrderEntity alloc] init];
        
        order._style = [_tfStyle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        order._color = [_tfColor.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        order._price = [_tfPrice.text floatValue];
        order._s = [_tfS.text intValue];
        order._m = [_tfM.text intValue];
        order._l = [_tfL.text intValue];
        order._xl = [_tfXL3.text intValue];
        order._1xl = [_tf1XL.text intValue];
        order._2xl = [_tf2XL.text intValue];
        order._3xl = [_tf3XL.text intValue];
        order._qty = order._s + order._m + order._l + order._xl + order._1xl + order._2xl + order._3xl;
        order._disc = [_tfDiscount.text intValue];
        order._total = order._qty * order._price;
        
        [ORDERLIST addObject:order];
        
        //[[JLToast makeText:@"New item was added successfully!" duration:1] show];
        
        
        for (int i = 83 ; i < 94 ; i++ ) {
            
            UITextField *tfds = (UITextField *) [self.view viewWithTag:i];
            tfds.text = @"";
        }
        
        NSString *pewPewPath = [[NSBundle mainBundle]pathForResource:@"tone" ofType:@"wav"];
        SystemSoundID soundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:pewPewPath], &soundID);
        AudioServicesPlaySystemSound(soundID);
//        AudioServicesDisposeSystemSoundID(soundID);
        //[[JLToast makeText:@"New item was added successfully!" duration:1] show];
        [self.navigationController.view makeToast:@"New item was added successfully!"];
    }
}

- (BOOL) isValid {
    
    if (_tfStyle.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INVALID_STYLECODE positive:ALERT_OK negative:nil];
        return NO;
    
    } else if (_tfPrice.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INVALID_PRICE positive:ALERT_OK negative:nil];
        return NO;
        
    } else if (_tfColor.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INVALID_COLOR positive:ALERT_OK negative:nil];
        return NO;
   
    }
//    else if (_tfS.text.length == 0 || _tfM.text.length == 0 || _tfM.text.length == 0 || _tfL.text.length == 0 || _tfXL3.text.length == 0 || _tf1XL.text.length == 0 || _tf2XL.text.length == 0 || _tf3XL.text.length == 0 || _tfM.text.length == 0) {
//        
//        [self showAlertDialog:ALERT_TITLE message:INVALID_QTY positive:ALERT_OK negative:nil];
//        return NO;
//    }
    
    return YES;
}

#pragma mark - keyboard touch actions

- (CGFloat)getOffsetYWhenShowKeybarod {
    
    if (deviceType == IPAD) {
        
        if ([_tfPPKCount isFirstResponder] || [_tfDiscount isFirstResponder] || [_tf3XL isFirstResponder] || [_tfXL3 isFirstResponder] || [_tfL isFirstResponder]) {
            
            return 200;
        }
        
        return 350;
    } else {
    
        if ([self.tfXL3 isFirstResponder]) {
            
            return 160.0f;
        } else if ([self.tfDiscount isFirstResponder]) {
            
            return 50.0f;
        
        } else if ([self.tfStyle isFirstResponder]) {
            
            return 240.0f;
        } else if ([self.tfPPKCount isFirstResponder]) {
            
            return 100.0f;
        } else if([self.tvNote isFirstResponder]) {
            
            return 40.0f;
        }
        
        return 210.0f;
    }
}

#pragma mark - user indifition function.

- (int) decrease : (int) value {
    
    value--;
    
    if (value > 0) return value;
    
    else return 0;
}

- (int) increase : (int) value {
    
    value ++;
    
    return value;
}

- (void) setPPK_1 : (int) count {
    
    selectedPPK = 1;
    
    self.tfS.text = [NSString stringWithFormat:@"%d", 1 * count];
    self.tfM.text = [NSString stringWithFormat:@"%d", 2* count];
    self.tfL.text = [NSString stringWithFormat:@"%d", 2* count];
    self.tfXL3.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf1XL.text = [NSString stringWithFormat:@"%d", 1 * count];
    self.tf2XL.text = [NSString stringWithFormat:@"%d", 0 * count];
    self.tf3XL.text = [NSString stringWithFormat:@"%d", 0 * count];
}

- (void) setPPK_2 : (int) count {
    
    selectedPPK = 2;
    
    self.tfS.text = [NSString stringWithFormat:@"%d", 1 * count];
    self.tfM.text = [NSString stringWithFormat:@"%d", 2* count];
    self.tfL.text = [NSString stringWithFormat:@"%d", 2* count];
    self.tfXL3.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf1XL.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf2XL.text = [NSString stringWithFormat:@"%d", 1 * count];
    self.tf3XL.text = [NSString stringWithFormat:@"%d", 0 * count];
    
}

- (void) setPPK_3 : (int) count {
    
    selectedPPK = 3;
    
    self.tfS.text = [NSString stringWithFormat:@"%d", 0 * count];
    self.tfM.text = [NSString stringWithFormat:@"%d", 0 * count];
    self.tfL.text = [NSString stringWithFormat:@"%d", 0 * count];
    self.tfXL3.text = [NSString stringWithFormat:@"%d", 0 * count];
    self.tf1XL.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf2XL.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf3XL.text = [NSString stringWithFormat:@"%d", 1 * count];
}

- (void) setPPK_4 : (int) count {
    
    selectedPPK = 4;
    
    self.tfS.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tfM.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tfL.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tfXL3.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf1XL.text = [NSString stringWithFormat:@"%d", 2 * count];
    self.tf2XL.text = [NSString stringWithFormat:@"%d", 0 * count];
    self.tf3XL.text = [NSString stringWithFormat:@"%d", 0 * count];
    
}



@end
