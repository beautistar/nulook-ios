//
//  SalesViewController.h
//  NuLook
//
//  Created by AOC on 25/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "QRCodeReaderDelegate.h"
#import "CustomerEntity.h"

@interface SalesViewController : CommonViewController <UITextFieldDelegate, QRCodeReaderDelegate>

@property (nonatomic, strong) CustomerEntity * _selectedCustomer;

@end
