//
//  CommonViewController.h
//  NuLook
//
//  Created by AOC on 08/01/17.
//  Copyright © 2017 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface CommonViewController : UIViewController {
    
    BOOL _isConnecting;
}

@property (nonatomic) float keyboardHeight;

- (void) showLoadingView;
- (void) showLoadingViewWithTitle:(NSString *) title;
- (void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay ;
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative;


@end
